<?php

return array(
	
	'netglue_route_layout' => array(
		
		// Whether we wish to enable the listener or not
		'enable' => true,
		
		'by_route' => array(
			// 'my-route-name' => 'file-path-or-name-of-layout-in-template-map'
		),
		
		'by_layout' => array(
			
			/**
			 * 'my-layout' => array(
			 * 	'route-1',
			 * 	'route-2',
			 * ),
			 */
			
		),
		
	),
	
);