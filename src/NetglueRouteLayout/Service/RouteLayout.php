<?php

namespace NetglueRouteLayout\Service;

class RouteLayout {
	
	/**
	 * Simple associative array of routeName=>layoutName
	 * @var array
	 */
	protected $routes = array();
	
	/**
	 * Constructor accepts an array defining routes.
	 * The array arg is searched for the keys
	 * 'by_layout' and 'by_route' - anything else is ignored
	 * @param array $config
	 * @return void
	 */
	public function __construct(array $config = array()) {
		if(count($config)) {
			if(isset($config['by_layout']) && is_array($config['by_layout'])) {
				foreach($config['by_layout'] as $layout => $routes) {
					$this->addRoutesByLayout($layout, $routes);
				}
			}
			if(isset($config['by_route']) && is_array($config['by_route'])) {
				foreach($config['by_route'] as $route => $layout) {
					$this->addRoute($route, $layout);
				}
			}
		}
	}
	
	/**
	 * Add multiple routes for the given layout
	 * @param string $layout
	 * @param array $routes
	 * @return self
	 */
	public function addRoutesByLayout($layout, array $routes) {
		foreach($routes as $route) {
			$this->addRoute($route, $layout);
		}
		return $this;
	}
	
	/**
	 * Set the layout for the given route name
	 * @param string $routeName
	 * @param string $layout
	 * @return self
	 * @throws \InvalidArgumentException if either arg is empty
	 */
	public function addRoute($routeName, $layout) {
		if(!is_string($routeName) || empty($routeName)) {
			throw new \InvalidArgumentException("Route must be a non-empty string");
		}
		if(!is_string($layout) ||empty($layout)) {
			throw new \InvalidArgumentException("Layout name must be a non-empty string");
		}
		$this->routes[$routeName] = $layout;
		return $this;
	}
	
	/**
	 * Return the layout specified for the given route
	 * @param string $routeName
	 * @return string
	 */
	public function getLayout($routeName) {
		$routeName = (string) $routeName;
		if(empty($routeName)) {
			return NULL;
		}
		if(isset($this->routes[$routeName])) {
			return $this->routes[$routeName];
		}
		return NULL;
	}
	
}