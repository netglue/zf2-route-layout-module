<?php
/**
 * Netglue Route Layout Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2013 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @link https://bitbucket.org/netglue/zf2-route-layout-module
 */

namespace NetglueRouteLayout;

use NetglueRouteLayout\Service\RouteLayout;

/**
 * Autoloader
 */
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;

/**
 * Config Provider
 */
use Zend\ModuleManager\Feature\ConfigProviderInterface;

/**
 * Bootstrap Listener
 */
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\EventManager\EventInterface as Event;
use Zend\Mvc\MvcEvent;

/**
 * Service Provider
 */
use Zend\ModuleManager\Feature\ServiceProviderInterface;

/**
 * Netglue Route Layout Module
 * @author George Steel <george@net-glue.co.uk>
 * @copyright Copyright (c) 2012 Net Glue Ltd (http://netglue.co)
 * @license http://opensource.org/licenses/MIT
 * @link https://bitbucket.org/netglue/zf2-route-layout-module
 */
class Module implements
	AutoloaderProviderInterface,
	ServiceProviderInterface,
	ConfigProviderInterface,
	BootstrapListenerInterface
{
	
	/**
	 * Return autoloader configuration
	 * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
	 * @return array
	 */
	public function getAutoloaderConfig() {
    return array(
			AutoloaderFactory::STANDARD_AUTOLOADER => array(
				StandardAutoloader::LOAD_NS => array(
					__NAMESPACE__ => __DIR__,
				),
			),
		);
	}
	
	/**
	 * Include/Return module configuration
	 * @return array
	 * @implements ConfigProviderInterface
	 */
	public function getConfig() {
		return include __DIR__ . '/../../config/module.config.php';
	}
	
	/**
	 * Return Service Config
	 * @return array
	 * @implements ServiceProviderInterface
	 */
	public function getServiceConfig() {
		return array(
			'factories' => array(
				'NetglueRouteLayout\Service\RouteLayout' => function($sm) {
					$config = $sm->get('Config');
					$options = NULL;
					if(isset($config['netglue_route_layout'])) {
						$options = $config['netglue_route_layout'];
					}
					return new RouteLayout($options);
				},
			),
		);
	}
	
	/**
	 * MVC Bootstrap Event
	 *
	 * @param Event $e
	 * @return void
	 * @implements BootstrapListenerInterface
	 */
	public function onBootstrap(Event $e) {
		$app = $e->getApplication();
		$serviceLocator = $app->getServiceManager();
		$config = $serviceLocator->get('Config');
		$config = isset($config['netglue_route_layout']) ? $config['netglue_route_layout'] : array();
		if(!isset($config['enable']) || $config['enable'] !== true) {
			return;
		}
		// Attach to the dispatch event triggered by action controllers
		$app->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array($this, 'onDispatch'), 100);
	}
	
	/**
	 * Switch layout if we can find a controller with the plugin() method
	 * @param MvcEvent $e
	 * @return void
	 */
	public function onDispatch(MvcEvent $e) {
		$app = $e->getApplication();
		$serviceLocator = $app->getServiceManager();
		$controller = $e->getTarget();
		$match = $e->getRouteMatch();
		$rls = $serviceLocator->get('NetglueRouteLayout\Service\RouteLayout');
		$layout = $rls->getLayout($match->getMatchedRouteName());
		if(NULL !== $layout) {
			if(method_exists($controller, 'plugin')) {
				$plugin = $controller->plugin('layout');
				$plugin->setTemplate($layout);
			}
		}
	}
}
