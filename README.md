# Switch Layout Based on Matched Route Name

## Introduction

This is a simple module to switch layout based on the currently matched route name

## Installation

The module should be installed with [composer](http://getcomposer.org). It's name is `netglue/zf2-route-layout-module`
If you have problems installing with composer, check your `minimum-stability` setting.

Enable the module in your main config file. The module name you should enter is `NetglueRouteLayout`

Look in the `vendor/netglue/zf2-route-layout-module` directory once installed and look at the config files to see what can be altered for your app.

## Configuration

By default, the module does nothing as there are no configured routes or layouts.

To specify route/layout changes provide config in the following way:

	//...
	
	'netglue_route_layout' => array(
		
		'by_route' => array(
			'my/route-name' => 'a-different-layout',
		),
		
		'by_layout' => array(
			'a-different-layout' => array(
				'my/route-name',
				'my/other-route-name',
			),
		),
		
	),
	
	//...

## Services

There is one service available from the service manager which is an instance of `NetglueRouteLayout\Service\RouteLayout` take a look in the `/src` dir... it's very simple.
	
	// Add a route manually
	$instance = $serviceLocator->get('NetglueRouteLayout\Service\RouteLayout');
	$instance->addRoute('my/route-name', 'a-different-layout');
	// Or
	$instance->addRoutesByLayout('new-layout', array(
		'my/route-name',
		'my/other-route-name'));
	// Find out which layout a particular route has. Returns null if no specific layout has been configured
	$layout = $instance->getLayout('my/route-name');
	

